<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Hamming Code">
    <meta name="author" content="Alexandru Orzanescu">
    <link rel="shortcut icon" href="">

    <title> CRC codification </title>

    <!-- Bootstrap core CSS 
    <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
  .footer
  {
  margin-bottom:10px;
  }
  h1, h3
  {
  text-align: center;
  }
  body
  {
  background-color:rgb(240, 240, 240);
  }
  hr
  {
  border-top: 1px solid #8597F5;
  }
  div.hr hr
  {
  margin-top:-10px;
  }
</style>
</head>

<body>
  <div class="container">
    <div class="row">
      <!-- first form: Codificatoarele CRC Sistematic si Standard -->
      <div class="col-sm-12">
        <h1> Codificatoarele CRC Sistematic si Standard </h1>
        <div class="hr col-sm-8 col-sm-offset-2" ><hr></div>
        <?php
          session_start();
          if(isset($_GET['err'])){
            $err = "";
            if($_GET['err']=="INVALID")
              $err .= "Datele sau codul contin caractere invalide! <br>";
            if($_GET['err']=="EMPTY")
              $err .= "Introduceti datele si codul! <br>";
          } else
            $err = "";

          if(isset($_SESSION['date']))
            $date = $_SESSION['date'];
          else 
            $date = "";
          if(isset($_SESSION['cod_gen']))
            $cod_gen = $_SESSION['cod_gen'];
          else
            $cod_gen = "";
          if(isset($_SESSION['crc_sis']))
            $crc_sis = $_SESSION['crc_sis'];
          else 
            $crc_sis = "";
          if(isset($_SESSION['crc_std']))
            $crc_std = $_SESSION['crc_std'];
          else 
            $crc_std = "";

          echo <<<EOT
            <form class="form-add-entry" role="form" action="cod_crc.php" method="post" enctype="multipart/form-data">
              <div class="col-sm-12"><font color='red'> $err </font></div>
            
              <div class="col-sm-5">
                <div class="form-group">
                  <label for="date">Introduceti cuvantul de date:</label>
                  <input type="text" class="form-control" name="date" id="date" value="$date" placeholder="ex. 1 10 101 1...">
                </div>
                <div class="form-group">
                  <label for="cod_gen">Codul generator:</label>
                  <input type="text" class="form-control" name="cod_gen" id="cod_gen" value="$cod_gen" 
                        placeholder="minim 1001 maxim grad 15 1..[0-1]..1">
                </div>
              </div>
              
              <div class="col-sm-5 col-sm-offset-1">
                <div class="form-group">
                  <label for="crc_sis">Codul CRC Sistematic:</label>
                  <input type="text" class="form-control" name="crc_sis" id="crc_sis" value="$crc_sis" readonly="readonly">
                </div>
                <div class="form-group">
                  <label for="crc_std">Codul CRC Standard:</label>
                  <input type="text" class="form-control" name="crc_std" id="crc_std" value="$crc_std" readonly="readonly">
                </div>
              </div>
              
              <div class="col-sm-12">
                <input type="submit" class="btn btn-primary btn-sm" name="submit" value="Codifica">
                <a href="clean_session.php"><button type="button" class="btn btn-primary btn-sm"> Initializeaza pagina </button></a>
              </div>
            </form>
EOT;
          
        ?>
      </div>
      <!-- second form: Decodificatoarele CRC sistematic si standard -->
      <div class="col-sm-12">
        <hr>
        <h1> Decodificatoarele CRC Sistematic si Standard </h1>
        <div class="hr col-sm-8 col-sm-offset-2" ><hr></div>
        <div class="col-sm-5">
          <h3> CRC Sistematic </h3>
          <?php 
            if(isset($_GET['sis_err'])){
              $sis_err = "";
              if($_GET['sis_err']=="INVALID")
                $sis_err .= "Cuvantul contine caractere invalide! <br>";
              if($_GET['sis_err']=="EMPTY")
                $sis_err .= "Introduceti cuvantul! <br>";
            } else
              $sis_err = "";

            if(isset($_SESSION['sis_cuv_cod']))
              $sis_cuv_cod = $_SESSION['sis_cuv_cod'];
            else 
              $sis_cuv_cod = "";
            if(isset($_SESSION['sis_cuv_decod']))
              $sis_cuv_decod = $_SESSION['sis_cuv_decod'];
            else 
              $sis_cuv_decod = "";
            if(isset($_SESSION['sis_rest']))
              $sis_rest = $_SESSION['sis_rest'];
            else 
              $sis_rest = "";
            if(isset($_SESSION['sis_error']))
              $sis_error = $_SESSION['sis_error'];
            else 
              $sis_error = "";

            echo <<<EOT
              <font color='red'> $sis_err </font>
              <form class="form-add-entry" role="form" action="decod_crc_sis.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="cod_gen" value="$cod_gen">
                <div class="form-group">
                  <label for="sis_cuv_cod">Introduceti cuvantul codificat:</label>
                  <input type="text" class="form-control" name="sis_cuv_cod" id="sis_cuv_cod" value="$sis_cuv_cod">
                </div>
                <input type="submit" class="btn btn-primary btn-sm" name="submit" value="Decodifica">
              </form>
            
            <!-- raspuns cod sistematic -->
            <div class="form-group">
              <label for="sis_cuv_decod">Cuvantul decodificat:</label>
              <input type="text" class="form-control" name="sis_cuv_decod" id="sis_cuv_decod" value="$sis_cuv_decod" readonly="readonly">
            </div>
            <div class="form-group">
              <label for="sis_rest">Restul:</label>
              <input type="text" class="form-control" name="sis_rest" id="sis_rest" value="$sis_rest" readonly="readonly">
            </div>
            <div class="form-group">
              <label for="sis_error">Eroarea detectata:</label>
              <input type="text" class="form-control" name="sis_error" id="sis_error" value="$sis_error" readonly="readonly">
            </div>
EOT;
          ?>
        </div>
        <div class="col-sm-5 col-sm-offset-1">
          <h3> CRC Standard </h3>
          <?php 
            if(isset($_GET['std_err'])){
              $std_err = "";
              if($_GET['std_err']=="INVALID")
                $std_err .= "Cuvantul contine caractere invalide! <br>";
              if($_GET['std_err']=="EMPTY")
                $std_err .= "Introduceti cuvantul! <br>";
            } else
              $std_err = "";

            if(isset($_SESSION['std_cuv_cod']))
              $std_cuv_cod = $_SESSION['std_cuv_cod'];
            else 
              $std_cuv_cod = "";
            if(isset($_SESSION['std_cuv_decod']))
              $std_cuv_decod = $_SESSION['std_cuv_decod'];
            else 
              $std_cuv_decod = "";
            if(isset($_SESSION['std_rest']))
              $std_rest = $_SESSION['std_rest'];
            else 
              $std_rest = "";
            if(isset($_SESSION['std_error']))
              $std_error = $_SESSION['std_error'];
            else 
              $std_error = "";

            echo <<<EOT
              <font color='red'> $std_err </font>
              <form class="form-add-entry" role="form" action="decod_crc_std.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="cod_gen" value="$cod_gen">
                <div class="form-group">
                  <label for="std_cuv_cod">Introduceti cuvantul codificat:</label>
                  <input type="text" class="form-control" name="std_cuv_cod" id="std_cuv_cod" value="$std_cuv_cod">
                </div>
                <input type="submit" class="btn btn-primary btn-sm" name="submit" value="Decodifica">
              </form>
            
            <!-- raspuns cod sistematic -->
            <div class="form-group">
              <label for="std_cuv_decod">Cuvantul decodificat:</label>
              <input type="text" class="form-control" name="std_cuv_decod" id="std_cuv_decod" value="$std_cuv_decod" readonly="readonly">
            </div>
            <div class="form-group">
              <label for="std_rest">Restul:</label>
              <input type="text" class="form-control" name="std_rest" id="std_rest" value="$std_rest" readonly="readonly">
            </div>
            <div class="form-group">
              <label for="std_error">Eroarea detectata:</label>
              <input type="text" class="form-control" name="std_error" id="std_error" value="$std_error" readonly="readonly">
            </div>
EOT;
          ?>
        </div>
      </div>
      <div class="footer col-sm-12">
        <hr>
        <div class="col-sm-6 col-sm-offset-3"> Creat de Alexandru Orzanescu si Mirona Popescu. </div>
      </div>
      <script src="jquery-2.1.0.min.js"> </script>
      <script src="bootstrap.min.js"> </script>
    </div>
  </div>

</body>
</html>