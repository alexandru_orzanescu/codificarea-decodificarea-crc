<?php
  session_start();
  //return url with: date cod_gen crc_sis crc_std
  if(isset($_POST['date'])){
    $date = $_POST['date'];
    $_SESSION['date'] = $_POST['date'];
  }
  if(isset($_POST['cod_gen'])){
    $cod_gen = $_POST['cod_gen'];
    $_SESSION['cod_gen'] = $_POST['cod_gen'];
  }

  if( strlen($date) > 0 && strlen($cod_gen) > 0 ){
			if(preg_match('/^[0-1]+$/', $date) != 1 || preg_match('/^1[0-1]+1$/', $cod_gen) != 1 ){	// daca avem alte caractere in afara de 0 si 1
				header("location: index.php?err=INVALID");
      } else {  // datele si codul sunt ok putem incepe aldoritmul de codificare
        /* 
        // crc sistematic - first try
        // det rang polinom generator X^n,n=rang
        // shifting la stanga cu rangul polinomului
        // X^rang * D(x) => C(x)
        // R(x) = C(x) mod G(x)
        // => E = CRC sitematic 
        */

        $size_D = strlen($date);
        $size_G = strlen($cod_gen) - 1;
        
        $D = str_split($date);
        $G = str_split($cod_gen);
        // shift la stanga
        for($i=0; $i < $size_G; $i++){
          $D[] = 0;
        }
        
        $crc_sis = $D;
        // operatia mod in modulo 2
        for($i=0; $i<$size_D; $i++){
          if($D[$i] == 0){
            for($j=$i;$j<=$size_G+$i; $j++)
              $D[$j] = bindec($D[$j]) ^ 0;
          } else {
            for($j=0; $j<=$size_G; $j++){
              $D[$i+$j] = bindec($D[$i+$j]) ^ bindec($G[$j]);
            }
          }
        }
        $rest = $D;
        // operatia add in modulo 2
        for($k=count($rest)-1, $l=count($crc_sis)-1; $k>=0; $k--, $l--){
          $crc_sis[$l] = bindec($crc_sis[$l]) ^ bindec($rest[$k]);
        }
        $_SESSION['crc_sis'] = implode($crc_sis);

        // crc standard E = D * G
        $D = str_split($date);
        $G = str_split($cod_gen);

        for($i=0; $i<$size_G+$size_D; $i++)
          $crc_std[] = 0;
        //operatia de inmultire in modulo 2 
        $rang=0;
        for($i=$size_G; $i>=0; $i--){
          if($G[$i] == 0){
            $rang++;
          } else {
            $to_add = $D;
            for($j=0; $j<$rang; $j++){
              $to_add[] = 0;
            }
            for($k=count($to_add)-1, $l=count($crc_std)-1; $k>=0; $k--, $l--){
              $crc_std[$l] = bindec($crc_std[$l]) ^ bindec($to_add[$k]);
            }
            $rang++;
          }
        }

        $_SESSION['crc_std'] = implode($crc_std);
 
        header("location: index.php");
      }
  } else {
    header("location: index.php?err=EMPTY");
  }
?>