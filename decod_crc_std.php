<?php
  session_start();
  //return url with: std_cuv_cod std_cuv_decod std_rest std_error
  if(isset($_POST['std_cuv_cod'])){
    $std_cuv_cod = $_POST['std_cuv_cod'];
    $_SESSION['std_cuv_cod'] = $_POST['std_cuv_cod'];
  } else
    $std_cuv_cod = "";

  if(isset($_POST['cod_gen'])){
    $cod_gen = $_POST['cod_gen'];
  } else {
    $cod_gen = "1001";  // valoarea minima / default
    $_SESSION['cod_gen'] = "1001";
  }

  if( strlen($std_cuv_cod) > 0 ){
			if( preg_match('/^[0-1]+$/', $std_cuv_cod) != 1 ){	// daca avem alte caractere in afara de 0 si 1
        $_SESSION['std_cuv_decod'] = "";
        $_SESSION['std_rest'] = "";
        $_SESSION['std_error'] = "";
				header("location: index.php?std_err=INVALID");
      } else { 
        $size_T = strlen($std_cuv_cod);
        $size_G = strlen($cod_gen) - 1;
        $size_D = $size_T - $size_G;
        // inaintea decodificarii
        $T = str_split($std_cuv_cod);
        $G = str_split($cod_gen);
        // operatia mod in modulo 2
        $cat;
        for($i=0; $i<$size_D; $i++){
          if($T[$i] == 0){
            for($j=$i;$j<=$size_G+$i; $j++)
              $T[$j] = bindec($T[$j])^0;
            $cat[] = 0;
          } else {
            for($j=0; $j<=$size_G; $j++)
              $T[ $i+$j ] = bindec($T[ $i+$j ]) ^ bindec($G[ $j ]);
            $cat[] = 1;
          }
        }
        $rest = implode($T);
        
        $D = implode($cat);
        
        if(bindec($rest) != 0)
          $err = "yes";
        else
          $err = "no";

        $_SESSION['std_cuv_decod'] = $D;
        $_SESSION['std_rest'] = $rest;
        $_SESSION['std_error'] = $err;
        header("location: index.php");
      }
  } else {
    $_SESSION['std_cuv_decod'] = "";
    $_SESSION['std_rest'] = "";
    $_SESSION['std_error'] = "";
    header("location: index.php?std_err=EMPTY");
  }
?>