/* CRC
 * Alexandru Orzanescu
 * Mirona Popescu
 */

CRC 2 metode:
	-sistematic
	-standard

Detectie erori la decodificare. 
Se observa bitii eronati in campul de rest.

Algoritmul -> pdf documentatie.
Toate operatiile se fac pe array-uri de 0 si 1.
Operatii implementate in modulo 2: div, mod, add.

Bibliografie

http://denethor.wlu.ca/pc364/CRCpre.html
http://denethor.wlu.ca/pc364/CRC.html