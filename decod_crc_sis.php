<?php
  session_start();
  //return url with: sis_cuv_cod sis_cuv_decod sis_rest sis_error
  if(isset($_POST['sis_cuv_cod'])){
    $sis_cuv_cod = $_POST['sis_cuv_cod'];
    $_SESSION['sis_cuv_cod'] = $_POST['sis_cuv_cod'];
  } else
    $sis_cuv_cod = "";

  if(isset($_POST['cod_gen'])){
    $cod_gen = $_POST['cod_gen'];
  } else {
    $cod_gen = "1001";  // valoarea minima / default
    $_SESSION['cod_gen'] = "1001";
  }

  if( strlen($sis_cuv_cod) > 0 ){
			if( preg_match('/^[0-1]+$/', $sis_cuv_cod) != 1 ){	// daca avem alte caractere in afara de 0 si 1
        $_SESSION['sis_cuv_decod'] = "";
        $_SESSION['sis_rest'] = "";
        $_SESSION['sis_error'] = "";
				header("location: index.php?sis_err=INVALID");
      } else {
        $size_T = strlen($sis_cuv_cod);
        $size_G = strlen($cod_gen) - 1;
        $size_D = $size_T - $size_G;
        // inaintea decodificarii
        $D_init = substr($sis_cuv_cod, 0, $size_D);
        $T = str_split($sis_cuv_cod);
        $G = str_split($cod_gen);
        // operatia mod in modulo 2
        for($i=0; $i<$size_D; $i++){
          if($T[$i] == 0){
            for($j=$i;$j<=$size_G+$i; $j++)
              $T[$j] = bindec($T[$j])^0;
          } else {
            for($j=0; $j<=$size_G; $j++)
              $T[ $i+$j ] = bindec($T[ $i+$j ]) ^ bindec($G[ $j ]);
          }
        }
        $rest = implode($T);

        if(bindec($rest) != 0)
          $err = "yes";
        else
          $err = "no";

        $_SESSION['sis_cuv_decod'] = $D_init;
        $_SESSION['sis_rest'] = $rest;
        $_SESSION['sis_error'] = $err;
        header("location: index.php");
      }
  } else {
    $_SESSION['sis_cuv_decod'] = "";
    $_SESSION['sis_rest'] = "";
    $_SESSION['sis_error'] = "";
    header("location: index.php?sis_err=EMPTY");
  }
?>